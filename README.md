# emailValidator
this is a bash script that uses nslookup to validate the domain name of each email address in the csv file that is received as an input with a mail list written on the rows.

## usage 
- clone the repo
- cd into the directory
- make the file executable with the next command:

´´´
chmod +x email_validator.sh
´´´

To use the script, run it and pass the input csv file as an argument:

´´´
./email_validator.sh email_list.csv
´´´

email_list.csv input file should look like this:


| john   | john@gmail.com |   |
| ------ | -------------- | - |
|thomposn| thomp@mail.com |   |
| jady   |  jady@some.com |   |
