#!/bin/bash
while read -r line; do
  email=$(echo $line | awk -F, '{print $2}')
  domain=$(echo $email | awk -F@ '{print $2}')
  if nslookup $domain > /dev/null; then
    echo -e "${email} \t${domain}\t \033[32m valid \033[0m"
  else
    echo -e "${email} \t${domain} \t\033[31m invalid \033[0m"
  fi
done < "$1"